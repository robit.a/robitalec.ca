---
title: "Etc"
---


<div class="row">
<div class="col-md-4">
<div class="panel panel-default">
<div class="panel-body">

[Photography](https://www.flickr.com/gp/51059352@N02/36Ajr9)
</div></div></div>

<!--
<div class="col-md-4">
<div class="panel panel-default">
<div class="panel-body">

Bread
</div></div></div>


<!--
<div class="col-md-4">
<div class="panel panel-default">	
<div class="panel-body">

a
</div></div></div> -->
</div>

<div class="row">

<div class="panel panel-default">
<div class="panel-body">

This site was built using [R Markdown](http://rmarkdown.rstudio.com/) and [GitLab Pages](https://gitlab.com/pages). Source code is available [here](https://gitlab.com/robitalec/robitalec.ca). 

The background image is in Yukon, Canada and &#169; Alec Robitaille (2018).  


</div></div></div>
